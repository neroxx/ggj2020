﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSpriteController : MonoBehaviour
{
    void Start()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();

        Vector3 screenSize = ScreenUtils.GetScreenSize();
        // It does not work with GetScreenPoint, I don't know wht??!!
        screenSize = Camera.main.ScreenToWorldPoint(screenSize);
        Vector2 spriteSize = spriteRenderer.sprite.bounds.size;

        Vector2 scale = transform.localScale;
        if (screenSize.x >= screenSize.y)
        { // Landscape (or equal)
            scale *= spriteSize.x / screenSize.x;
        }
        else
        { // Portrait
            scale *= spriteSize.y / screenSize.y;
        }

        transform.position = Vector2.zero; // Optional
        transform.localScale = scale;
    }
}
