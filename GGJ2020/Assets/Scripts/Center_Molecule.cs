﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Center_Molecule : MonoBehaviour
{

    public Vector3 rotateVector = new Vector3(0.0f, 0.0f, 0.0f);
    bool finishFlag = false;
    int total = 1;
    
   
    ProteinEvent proteinEvent = new ProteinEvent();
    public proteins proteinType;

    // Start is called before the first frame update
    void Start()
    {
        
        ProteinManager.AddInvoker(this);
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.Rotate(0, 0, 30 * Time.deltaTime);

        ElementController[] allChildren = gameObject.GetComponentsInChildren<ElementController>();

        foreach(ElementController child in allChildren)
        {
            total += child.GetComponent<ElementController>().remaning;
        }

        if (total == 0 && finishFlag == false)
        {
            
            boom();

        }

        total = 0;

    }
    public void boom()
    {
        finishFlag = true;
        

        proteinEvent.Invoke(proteinType);
        Debug.Log("Molecule Finished! Flag set.");
        Destroy(gameObject);
    }

    internal void ProteinEventAddListener(UnityAction<proteins> listener)
    {
        proteinEvent.AddListener(listener);
    }
}
