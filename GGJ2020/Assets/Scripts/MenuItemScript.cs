﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuItemScript : MonoBehaviour
{
    int xRotation;
    int yRotation;
    int zRotation;
    void Start()
    {
        zRotation = Random.Range(5, 45);
    }
    void Update()
    {
        this.gameObject.transform.Rotate(0, 0, zRotation * Time.deltaTime);
    }
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
