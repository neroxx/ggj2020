﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FixedLadyButton : MonoBehaviour
{
    private void OnMouseUp()
    {
        SceneManager.LoadScene("ExitScreen", LoadSceneMode.Single);
    }
}
