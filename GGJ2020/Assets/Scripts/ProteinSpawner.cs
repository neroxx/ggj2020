﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProteinSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject[] proteins;
    [SerializeField]
    GameObject spawnPoint;
    AudioClip moleculefinished;
    AudioSource sound;

    bool firstMol;
    // Start is called before the first frame update
    void Start()
    {
        sound = this.gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        moleculefinished = (AudioClip)Resources.Load("moleculefinished");
        
        proteins mol = ProteinManager.proteinList[ProteinManager.index];
        //Debug.Log(mol);
        firstMol = true;
        CreateMol((int)mol);
        firstMol = false;

        ProteinManager.AddCreateMolListener(CreateMol);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateMol(int molecule)
    {
        GameObject moleculeInstance = Instantiate(proteins[molecule], spawnPoint.transform.position, Quaternion.identity, transform);
        moleculeInstance.GetComponent<Center_Molecule>().proteinType = (proteins)molecule;
        PlaySound();
    }

    public void DestroyMol()
    {
        transform.GetChild(0).GetComponent<Center_Molecule>().boom();
    }
        
    void PlaySound()
    {
        if(!firstMol)
            sound.PlayOneShot(moleculefinished);
    }

    
}
