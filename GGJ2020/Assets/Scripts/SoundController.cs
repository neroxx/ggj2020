﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundController : MonoBehaviour
{
    AudioSource audioSource;
    [SerializeField]
    AudioClip buttonClick;
    [SerializeField]
    AudioClip intro;

    private static SoundController instance = null;

    // Game Instance Singleton
    public static SoundController Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);

    }

    void Start()
    {
        audioSource = Instance.GetComponent<AudioSource>();
        StartIntro();
    }

    // Update is called once per frame
    public void ButtonClicked()
    {
        Instance.audioSource.PlayOneShot(buttonClick);
    }

    public void StartIntro()
    {
        Instance.audioSource.clip = intro;
        Instance.audioSource.Play();
    }
}
