﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementController : MonoBehaviour
{
    public bool filled = false;
    TMPro.TextMeshPro elementText;
    public int H, N, C, O;
    private int Hi, Ni, Ci, Oi;
    public bool Hb, Nb, Cb, Ob;
    public int remaning;
    AudioSource sound;
    AudioClip sticksound;
    AudioClip breaksound;

    string HString, NString, OString, CString, FinalString;

    // Start is called before the first frame update
    void Start()
    {
        sound = this.gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        sticksound = (AudioClip)Resources.Load("elementstick");
        breaksound = (AudioClip)Resources.Load("elementbreak");


    Hi = H;
        Ni = N;
        Ci = C;
        Oi = O;

        elementText = GetComponent<TMPro.TextMeshPro>();
        elementText.richText = true;

        Debug.Log("Start");
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);

        remaning = H + N + C + O;
    }

    void resetCount()
    {
        if (Hb == true)
            H = Hi;
        if (Nb == true)
            N = Ni;
        if (Cb == true)
            C = Ci;
        if (Ob == true)
            O = Oi;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("Collision!");

        if (other.gameObject.tag == "H" && Hb == true)
        {
            H -= 1;
            sound.PlayOneShot(sticksound);
        }
        else if (other.gameObject.tag == "H" && Hb == false)
        {
            resetCount();
            sound.PlayOneShot(breaksound);
        }

        if (other.gameObject.tag == "N" && Nb == true)
        {
            N -= 1;
            sound.PlayOneShot(sticksound);
        }
        else if (other.gameObject.tag == "N" && Nb == false)
        {
            resetCount();
            sound.PlayOneShot(breaksound);
        }

        if (other.gameObject.tag == "O" && Ob == true)
        {
            O -= 1;
            sound.PlayOneShot(sticksound);
        }
        else if (other.gameObject.tag == "O" && Ob == false)
        {
            resetCount();
            sound.PlayOneShot(breaksound);
        }

        if (other.gameObject.tag == "C" && Cb == true)
        {
            C -= 1;
            sound.PlayOneShot(sticksound);
        }
        else if (other.gameObject.tag == "C" && Cb == false)
        {
            resetCount();
            sound.PlayOneShot(breaksound);
        }


        if (H == -1 || N == -1 || O == -1 || C == -1)
        {
            resetCount();
            sound.PlayOneShot(breaksound);
        }
            
        //////////////////// H
        if (Hb == true)
        {

            if (H == 3)
            {
                HString = "<color=#2F2F2F>H<sub>" + H.ToString() + "</sub></color>";
            }
            else if (H == 2)
            {
                HString = "<color=#2F2F2F>H<sub>" + H.ToString() + "</sub></color>";
            }
            else if (H == 1)
            {
                HString = "<color=#2F2F2F>H</color>";
            }
            else if (H == 0)
            {
                HString = "<color=#FFFFFF>H</color>";
            }
        }
        //////////////////// N
        if (Nb == true)
        {
            if (N == 1)
            {
                NString = "<color=#2F2F2F>N</color>";
            }
            else if (N == 0)
            {
                NString = "<color=#FFFFFF>N</color>";
            }
        }
        //////////////////// O
        if (Ob == true)
        {
            if (O == 1)
            {
                OString = "<color=#2F2F2F>O</color>";
            }
            else if (O == 0)
            {
                OString = "<color=#FFFFFF>O</color>";
            }
        }
        //////////////////// C
        if (Cb == true)
        {
            if (C == 1)
            {
                CString = "<color=#2F2F2F>C</color>";
            }
            else if (C == 0)
            {
                CString = "<color=#FFFFFF>C</color>";
            }
        }
        FinalString = CString + OString + NString + HString;

        elementText.text = FinalString;

        Destroy(other.gameObject);
    }
}