﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementDestroyer : MonoBehaviour
{
    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
