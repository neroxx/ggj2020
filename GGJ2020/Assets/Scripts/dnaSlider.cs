﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dnaSlider : MonoBehaviour
{
    SpriteRenderer renderer;
    float t = 0;
    bool loadFinished = false;
    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!loadFinished)
            LoadDNA();
    }

    void LoadDNA()
    {
        t += 0.1f * Time.fixedDeltaTime;
        float lp = Mathf.Lerp(0, 35, t);
        if (renderer.size.y < 35)
            renderer.size = new Vector2(renderer.size.x, lp);
        else
            loadFinished = true;
    }
}
