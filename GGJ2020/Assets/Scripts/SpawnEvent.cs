﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpawnEvent : UnityEvent<int>
{
    
}

public class ProteinEvent : UnityEvent<proteins>
{

}

public class MolEvent : UnityEvent<int>
{

}