﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelectionController : MonoBehaviour
{
    [SerializeField]
    Character[] character;
    [SerializeField]
    List<GameCharacter> gameCharacter = new List<GameCharacter>();
    [SerializeField]
    GameObject characterTemplate;

    GameObject gameController;
    CanvasController canvasController;
    int level = 0;

    // Start is called before the first frame update
    void Start()
    {
        canvasController = GetComponent<CanvasController>();
        gameController = GameObject.FindGameObjectWithTag("GameController");

        SetCharacterButtons();
    }

    void SetCharacterButtons()
    {
        Transform characterCanvas = canvasController.characterCanvas.transform;
        for (int i = 0; i < characterCanvas.childCount; i++)
        {
            Transform characterButton = characterCanvas.GetChild(i);

            if(null != character[i])
            {
                // Create the template of Game Character
                GameCharacter tempGameCharacter = new GameCharacter(character[i]);
                tempGameCharacter.SetPrefab(characterButton.gameObject);

                // Append to the game character list
                gameCharacter.Add(tempGameCharacter);
                // Update the game character state
                UpdateState(tempGameCharacter);

                AddButtonListenerWithCharacter(characterButton.GetComponent<Button>(), tempGameCharacter);
            }
        }
    }

    void LoadCharacterButtons()
    {
        foreach (Character tempCharacter in character)
        {
            // Create the template of Game Character
            GameCharacter tempGameCharacter = new GameCharacter(tempCharacter);
            // Create game object
            GameObject prefab = Instantiate(characterTemplate, canvasController.characterCanvas.transform);
            tempGameCharacter.SetPrefab(prefab);

            // Append to the game character list
            gameCharacter.Add(tempGameCharacter);
            // Update the game character state
            UpdateState(tempGameCharacter);
        }
    }

    CharacterState UpdateState(GameCharacter updateCharacter)
    {
        // If level is already passed set as healed
        if (level > gameCharacter.IndexOf(updateCharacter))
        {
            updateCharacter.SetState(CharacterState.healed);
        }
        // If level is current it is just unlocked
        else if (level == gameCharacter.IndexOf(updateCharacter))
        {
            updateCharacter.SetState(CharacterState.unlocked);
        }
        // If level is not accessed, then lock
        else
        {
            updateCharacter.SetState(CharacterState.locked);
        }

        return updateCharacter.GetState();
    }

    bool isPlayable(GameCharacter updateCharacter)
    {
        bool playable = true;
       CharacterState state = updateCharacter.GetState();

        if (state == CharacterState.locked)
            playable = false;

        return playable;
    }

    public void StartLevel(GameCharacter playCharacter)
    {
        if(isPlayable(playCharacter))
        {
            gameController.GetComponent<SoundController>().ButtonClicked();
            StartLevel();
        }
        // Todo: add some effect maybe?
    }
    
    void StartLevel()
    {
        gameController.GetComponent<GameController>().LoadScene("ElementRespawner");
    }

    void AddButtonListenerWithCharacter(Button characterButton, GameCharacter listenerCharacter)
    {
        characterButton.onClick.AddListener( () => StartLevel(listenerCharacter));
    }

    void Unlock(int charIndex)
    {
        gameCharacter[charIndex].SetState(CharacterState.unlocked);
    }

    void Heal(int charIndex)
    {
        gameCharacter[charIndex].SetState(CharacterState.healed);
    }
}
