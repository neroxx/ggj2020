﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CharacterButtonController : MonoBehaviour
{
    public GameObject gameController;
    // Start is called before the first frame update
    void Start()
    {
        Button button = GetComponent<Button>();
        gameController = GameObject.FindGameObjectWithTag("GameController");
        ButtonController buttonController = gameController.GetComponent<ButtonController>();
        SoundController soundController = gameController.GetComponent<SoundController>();

        //button.onClick.AddListener(buttonController.StartLevel);
        button.onClick.AddListener(soundController.ButtonClicked);
    }
}
