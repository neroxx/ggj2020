﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    GameController gameController;
    BubbleController bubbleController;
    [SerializeField]
    GameObject eventSystem;

    private static ButtonController instance = null;

    // Game Instance Singleton
    public static ButtonController Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(ButtonController)) as ButtonController;
            return instance;
        }

        set
        {
            instance = value;
        }
    }


    public void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void Start()
    {
        //DontDestroyOnLoad(Instance.eventSystem);

        //Instance.gameController = Instance.GetComponent<GameController>();
        //Instance.bubbleController = Instance.GetComponent<BubbleController>();
    }

    public void StartButton()
    {
        Instance.gameController.LoadScene("CharacterSelection");
        // This could be sperated from button controller to bubble controller or game controller
        Instance.bubbleController.StopBubbles();
    }

    public void SettingsButton()
    {

    }

    public void ExitButton()
    {
        Instance.gameController.LoadScene("Crew");
        Application.Quit();
    }

    public void MenuButton()
    {
        Instance.gameController.LoadScene("Menu");
    }
}
