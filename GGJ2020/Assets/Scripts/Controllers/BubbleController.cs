﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleController : MonoBehaviour
{
    #region SerializeVariables
    // Reference to the Prefab.
    [SerializeField]
    GameObject[] bubblePrefabs;
    [SerializeField]
    int numberOfBubbles = 30;
    [SerializeField]
    float bubbleSpeedFactor;
    [SerializeField]
    Vector2 bubbleSize;
    [SerializeField]
    Vector2 bubbleRotationTorqueRange;
    [SerializeField]
    Vector2 spawnLocationStart;
    [SerializeField]
    Vector2 spawnLocationEnd;
    [SerializeField]
    float bubbleSpawnTime = 2f;
    [SerializeField]
    Transform bubbleRoot;
    #endregion //SerializeVariables

    #region PrivateVariables
    // Enable/Disable bubble spawn process
    bool bubbleFactory = true;
    // Bubble spawn interval time
    float bubbleCountDown;
    Vector3 screenPoint;
    #endregion //PrivateVariables


    #region Singleton
    private static BubbleController instance = null;

    // Game Instance Singleton
    public static BubbleController Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);

    }
    #endregion //Singleton



    // Start is called before the first frame update
    void Start()
    {
        screenPoint = ScreenUtils.GetScreenSize();
        if (null == bubbleRoot)
        {
            bubbleFactory = false;
        }
        else
        {
            CreateRandomBubbles();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Instance.bubbleFactory & null!= bubbleRoot)
        {
            bubbleCountDown -= Time.deltaTime;
            if (0 >= bubbleCountDown)
            {
                ResetBubbleCountdown();
                CreateNewBubble(RandomSpawnPointOnSpawnLocation());
            }
        }
    }

    void CreateRandomBubbles()
    {
        for (int i = 0; i < numberOfBubbles; i++)
        {
            CreateNewBubble(RandomSpawnPointOnScreen());
        }
    }

    void CreateNewBubble(Vector3 spawnPoint)
    {
        float tempBubbleSize = Random.Range(bubbleSize.x, bubbleSize.y);
        float moveFactorOffset = Random.Range(-0.1f, 0.1f);
        float bubbleSpeed = bubbleSpeedFactor * (moveFactorOffset + tempBubbleSize);
        GameObject bubble = CreateBubbleInstance(spawnPoint);
        ScaleBubble(bubble, tempBubbleSize);
        MoveBubble(bubble, bubbleSpeed);
    }

    GameObject CreateBubbleInstance(Vector3 spawnPoint)
    {
        int bubbleIndex = Random.Range(0, bubblePrefabs.Length);
        GameObject bubble = Instantiate(bubblePrefabs[bubbleIndex], spawnPoint, Quaternion.Euler(0, 0, Random.Range(-180,180)), bubbleRoot);
        return bubble;
       
    }

    void ScaleBubble(GameObject bubble, float scaleFactor)
    {
        bubble.transform.localScale = new Vector3(scaleFactor, scaleFactor, 1);
    }

    void MoveBubble(GameObject bubble, float moveFactor)
    {
        Rigidbody2D rigidbody = bubble.GetComponent<Rigidbody2D>();
        float tempBubbleTorque = Random.Range(bubbleRotationTorqueRange.x, bubbleRotationTorqueRange.y);
        rigidbody.AddForce(new Vector2(Random.Range(-0.2f * moveFactor, 0.2f * moveFactor), 2.0f * moveFactor), ForceMode2D.Impulse);
        rigidbody.AddTorque(tempBubbleTorque);
    }

    Vector3 RandomSpawnPointOnScreen()
    {
        return new Vector3(Random.Range(0, screenPoint.x), Random.Range(spawnLocationEnd.y, screenPoint.y), 0);
    }

    Vector3 RandomSpawnPointOnSpawnLocation()
    {
        return new Vector3(Random.Range(screenPoint.x*spawnLocationStart.x, screenPoint.x * spawnLocationEnd.x), Random.Range(screenPoint.y * spawnLocationStart.y, screenPoint.y * spawnLocationEnd.y), 0);
    }

    public void StopBubbles()
    {
        BubbleFactory(false);
        RemoveBubbles();
    }

    void BubbleFactory(bool state)
    {
        Instance.bubbleFactory = state;
    }

    void RemoveBubbles()
    {
        for(int i = 0; bubbleRoot.childCount > i; i++)
        {
            GameObject bubble = Instance.bubbleRoot.GetChild(i).gameObject;
            Destroy(bubble);
        }
    }

    void ResetBubbleCountdown()
    {
        bubbleCountDown = bubbleSpawnTime;
    }
}
