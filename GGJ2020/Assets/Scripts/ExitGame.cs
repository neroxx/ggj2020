﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGame : MonoBehaviour
{
    [SerializeField]
    float count;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        count += Time.fixedDeltaTime;
        if (count > 10.0f)
        {
            Debug.Log("Quit");
            Application.Quit();
        }
    }
}
