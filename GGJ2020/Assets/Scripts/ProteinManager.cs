﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class ProteinManager
{
    static public List<proteins> proteinList;
    static public List<proteins> proteinCounterList;
    static public int index = 0;
    static public int indexCounter = 0;

    static MolEvent createMolListener = new MolEvent();
    
    static public void GetLists(List<proteins> org, List<proteins> counter)
    {
        proteinList = org;
        proteinCounterList = counter;
    }

    static public void createMol(int helixSide)
    {
        if(helixSide == 0)
        {
            Debug.Log("helixSide = 0 " + indexCounter);

            if (indexCounter >= proteinCounterList.Count)
            {
                index = 0;
                indexCounter = 0;
                UnityEngine.SceneManagement.SceneManager.LoadScene("FixedLady", UnityEngine.SceneManagement.LoadSceneMode.Single);
            }
            else
                createMolListener.Invoke((int)proteinCounterList[indexCounter]);

            index++;
        }
        else if(helixSide == 1)
        {
            Debug.Log("helixSide = 1 " + index);

            if (index >= proteinList.Count)
            {
                index = 0;
                indexCounter = 0;
                UnityEngine.SceneManagement.SceneManager.LoadScene("FixedLady", UnityEngine.SceneManagement.LoadSceneMode.Single);
            }
            else
                createMolListener.Invoke((int)proteinList[index]);

            indexCounter++;
        }
    }

    static Center_Molecule invoker;
    static UnityAction<proteins> listener;

    /// <summary>
    /// Adds the invoker
    /// </summary>
    /// <param name="script">fish script</param>
    public static void AddInvoker(Center_Molecule script)
    {
        invoker = script;
        if (listener != null)
        {
            invoker.ProteinEventAddListener(listener);
        }
    }

    /// <summary>
    /// Adds the listener
    /// </summary>
    /// <param name="handler">event handler</param>
    public static void AddListener(UnityAction<proteins> handler)
    {
        listener = handler;
        if (invoker != null)
        {
            invoker.ProteinEventAddListener(listener);
        }
    }
    public static void AddCreateMolListener(UnityAction<int> handler)
    {
        createMolListener.AddListener(handler);
    }

    
}
