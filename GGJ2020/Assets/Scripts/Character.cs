﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CharacterState
{
    locked,
    unlocked,
    healed
}

[CreateAssetMenu(fileName = "Character", menuName = "Create Character")]
public class Character: ScriptableObject
{
    [SerializeField]
    public Sprite mutantImage;
    [SerializeField]
    public Sprite normalImage;
    [SerializeField]
    public Sprite questionImage;
    public CharacterState state;
    // Todo: Add genetic code of character
}

public class GameCharacter
{
    GameObject prefab;
    Character character;

    public GameCharacter(Character newCharacter)
    {
        character = newCharacter;
    }

    public CharacterState GetState()
    {
        return character.state;
    }

    public void SetState(CharacterState newState)
    {
        character.state = newState;
        SetImage();
    }

    public void SetImage()
    {
        switch(character.state)
        {
            case CharacterState.locked:
                SetPrefabImage(character.questionImage);
                break;
            case CharacterState.unlocked:
                SetPrefabImage(character.mutantImage);
                break;
            case CharacterState.healed:
                SetPrefabImage(character.normalImage);
                break;
            default:
                SetPrefabImage(character.questionImage);
                break;
        }
    }

    public void SetPrefab(GameObject newPrefab)
    {
        this.prefab = newPrefab;
    }

    void SetPrefabImage(Sprite image)
    {
        prefab.GetComponent<Image>().sprite = image;
    }
}