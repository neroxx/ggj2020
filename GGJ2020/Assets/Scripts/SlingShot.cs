﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SlingShot : MonoBehaviour
{
    [SerializeField]
    Vector2 screenTouchPoint;
    [SerializeField]
    float radius = 1;
    // The Force added upon release
    public float force = 1300;
    // Describe elemnet location
    public int elementLocation;
    AudioSource throwsound;

    LineRenderer lineRenderer;
    [SerializeField]
    AnimationCurve animCurve;

    SpawnEvent spawnEvent = new SpawnEvent();

    void Start()
    {
        if (lineRenderer == null)
        {
            lineRenderer = GetComponent<LineRenderer>();
        }
        lineRenderer.positionCount = 0;
        SpawnManager.AddInvoker(this);
        throwsound = this.gameObject.GetComponent<AudioSource>();
    }

    void OnMouseDown()
    {
        screenTouchPoint = transform.position;

        lineRenderer.widthCurve = animCurve;
        //lineRenderer.material;

        lineRenderer.positionCount = 1;
        lineRenderer.SetPosition(0, screenTouchPoint);
    }

    void OnMouseDrag()
    {
        // Convert mouse position to world position
        Vector2 p = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // Keep it in a certain radius
        Vector2 dir = p - screenTouchPoint;
        if (dir.sqrMagnitude > radius)
            dir = dir.normalized * radius;

        // Set the Position
        transform.position = screenTouchPoint + dir;

        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(1, transform.position);
    }
    void OnMouseUp()
    {
        // Disable isKinematic
        GetComponent<Rigidbody2D>().isKinematic = false;

        // Add the Force
        Vector2 dir = screenTouchPoint - (Vector2)transform.position;
        GetComponent<Rigidbody2D>().AddForce(dir * force);

        // Spawn new element
        spawnEvent.Invoke(elementLocation);

        lineRenderer.positionCount = 0;
        // Remove the Script (not the gameObject)
        throwsound.Play();
        Destroy(this);
    }

    public void SpawnEventAddListener(UnityAction<int> listener)
    {
        spawnEvent.AddListener(listener);
    }
}
