﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MutantLady : MonoBehaviour
{
    AudioSource sound;
    AudioClip clicksound;
    AudioClip oversound;

    void Start()
    {
        sound = this.gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        clicksound = (AudioClip)Resources.Load("clicksound");
    }

    private void OnMouseUp()
    {
        SceneManager.LoadScene("ElementRespawner", LoadSceneMode.Single);
    }

    private void OnMouseDown()
    {
        sound.PlayOneShot(clicksound);
    }
}
