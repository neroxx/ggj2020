﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

static public class SpawnManager
{
    static SlingShot invoker;
    static UnityAction<int> listener;

    /// <summary>
    /// Adds the invoker
    /// </summary>
    /// <param name="script">fish script</param>
    public static void AddInvoker(SlingShot script)
    {
        invoker = script;
        if (listener != null)
        {
            invoker.SpawnEventAddListener(listener);
        }
    }

    /// <summary>
    /// Adds the listener
    /// </summary>
    /// <param name="handler">event handler</param>
    public static void AddListener(UnityAction<int> handler)
    {
        listener = handler;
        if (invoker != null)
        {
            invoker.SpawnEventAddListener(listener);
        }
    }

    static public void ElementIsThrown(int location)
    {
        ElementRespawner eS = new ElementRespawner();
        eS.SpawnElement(location);
    }
}
