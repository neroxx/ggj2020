﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScript : MonoBehaviour
{
    private void OnMouseUp()
    {
        SceneManager.LoadScene("Page3", LoadSceneMode.Single);
    }
}
