﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class dnaCreator : MonoBehaviour
{
    [SerializeField]
    GameObject proteinsObj;
    [SerializeField]
    GameObject proteinCountersObj;
    [SerializeField]
    Material[] materials;
    [SerializeField]
    Material[] materialsBroken;
    [SerializeField]
    Mesh brokenMesh;
    [SerializeField]
    Mesh repairedMesh;

    [SerializeField]
    float rotSpeed = 0.1f;
    [SerializeField]
    GameObject dna;

    int proteinRepairCounter = 0;

    List<proteins> proteinList = new List<proteins>();
    List<proteins> proteinCounterList = new List<proteins>();

   

    void Awake()
    {
        CreateDNAHelix();
        ProteinManager.GetLists(proteinList, proteinCounterList);
    }
    // Start is called before the first frame update
    void Start()
    {
        ProteinManager.AddListener(Repair);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.right, rotSpeed * Time.fixedDeltaTime);
    }

    void Repair(proteins protein)
    {
        Debug.Log("Repair!");
        proteinRepairCounter++;
        if(proteinRepairCounter % 2 == 1)
        {
            Transform childObj = proteinsObj.transform.GetChild(ProteinManager.index);
            childObj.GetComponent<MeshFilter>().mesh = repairedMesh;
            childObj.GetComponent<MeshRenderer>().material = materials[(int)proteinList[ProteinManager.index]];
            ProteinManager.createMol(0);
            
        }
        else if (proteinRepairCounter % 2 == 0)
        {
            Transform childObj = proteinCountersObj.transform.GetChild(ProteinManager.indexCounter);
            childObj.GetComponent<MeshFilter>().mesh = repairedMesh;
            childObj.GetComponent<MeshRenderer>().material = materials[(int)proteinCounterList[ProteinManager.indexCounter]];
            ProteinManager.createMol(1);
        }
        
        
    }

    void CreateDNAHelix()
    {
        for(int child=0; child < proteinsObj.transform.childCount; child++)
        {
            int random = Random.Range(0, 4);
            proteins protein = (proteins)random;
            proteinList.Add(protein);

            Transform childObj = proteinsObj.transform.GetChild(child);
            childObj.GetComponent<MeshRenderer>().material = materialsBroken[(int)protein];

            childObj.GetComponent<MeshFilter>().mesh = brokenMesh;
        }

        CreateCounterDNA(proteinList);
    }

    void CreateCounterDNA(List<proteins> orgList)
    {
        foreach (proteins orgProtein in orgList)
        {
            if (orgProtein == proteins.adenine)
            {
                proteinCounterList.Add(proteins.thymine);
            }
            else if (orgProtein == proteins.thymine)
            {
                proteinCounterList.Add(proteins.adenine);
            }
            else if (orgProtein == proteins.guanine)
            {
                proteinCounterList.Add(proteins.cytosine);
            }
            else if (orgProtein == proteins.cytosine)
            {
                proteinCounterList.Add(proteins.guanine);
            }
        }

        for (int child = 0; child < proteinCountersObj.transform.childCount; child++)
        {
            Transform childObj = proteinCountersObj.transform.GetChild(child);
            childObj.GetComponent<MeshFilter>().mesh = brokenMesh;
            childObj.GetComponent<MeshRenderer>().material = materialsBroken[(int)proteinCounterList[child]];
        }
    }

    
}

public enum proteins
{
    adenine = 0,
    thymine,
    guanine,
    cytosine
}