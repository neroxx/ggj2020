﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementRespawner : MonoBehaviour
{
    // Reference to the Prefab.
    public GameObject[] preFabs;
    // Locations of elements
    public GameObject ElementLocation;
    // Instances of elements
    public GameObject[] preFabsInstance;

    public int prevRandomValue = 0;
    int randomValue = 0;

    /// <summary>
    /// Spawn how many element we need
    /// </summary>
    void Start()
    {
        for(int i= 0; i<ElementLocation.transform.childCount; i++)
        {
            InstantiatePreFabs(i);
        }

        SpawnManager.AddListener(SpawnElement);
    }

    /// <summary>
    /// Spawn one element on given location
    /// </summary>
    /// <param name="location">Location of element</param>
    public void SpawnElement(int location)
    {
        StartCoroutine(ExecuteAfterTime(0.5f, location));
    }

    /// <summary>
    /// Generate the random value and instantiate the preFab
    /// </summary>
    /// <param name="instanceNumber"></param>
    void InstantiatePreFabs(int instanceNumber)
    {
        while(prevRandomValue == randomValue)
        {
            randomValue = Random.Range(0, preFabs.Length);
        }
        preFabsInstance[instanceNumber] = Instantiate(preFabs[randomValue], ElementLocation.transform.GetChild(instanceNumber).position, Quaternion.identity);
        preFabsInstance[instanceNumber].GetComponent<SlingShot>().elementLocation = instanceNumber;
        prevRandomValue = randomValue;
    }

    IEnumerator ExecuteAfterTime(float time, int location)
    {
        yield return new WaitForSeconds(time);

        InstantiatePreFabs(location);
    }
}
