﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScreenUtils
{
    public static Vector3 GetScreenPoint()
    {
        Vector3 size = GetScreenPoint();
        Vector3 point = Camera.main.ScreenToWorldPoint(size);
        return point;
    }

    public static Vector3 GetScreenSize()
    {
        Vector3 size = new Vector3(Screen.width, Screen.height, 0);

        return size;
    }
}
